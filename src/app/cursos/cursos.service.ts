import { EventEmitter, Injectable } from '@angular/core';
import { ICurso } from '../shared/interface/icurso';
import { LogService } from '../shared/services/log.service';


@Injectable()
export class CursosService
{

	static parteCompartilhada = new EventEmitter<ICurso>();
	emitirCursoCriado = new EventEmitter<ICurso>();

	// Inicializar a variável é muito importante
	private cursos: ICurso[] = [
		{ id: 1, nome: 'PHP' },
		{ id: 2, nome: 'Marketing' },
		{ id: 3, nome: 'Java' }
	];


	constructor ( protected logService: LogService )
	{
		console.log( 'Recebendo chamado...' );
		this.logService.consoleLog( 'Log personalizado monitorando...' );
	}

	getCursos (): ICurso[]
	{
		this.logService.consoleLog( 'Cursos sendo requisitados!' );
		return this.cursos;
	}

	getCurso ( id: number ): ICurso
	{
		let curso: ICurso;
		for ( let i = 0; i < this.cursos.length; i++ )
		{
			curso = this.cursos[ i ];
			if ( curso.id == id )
			{
				return curso;
			}
		}
	}

	addCurso ( curso: ICurso )
	{
		this.logService.consoleLog( 'this.logService - CursosService: ' + curso.nome );
		console.log( 'Console.log - CursosService: ' + curso.nome );
		this.cursos.push( curso );
		this.emitirCursoCriado.emit( curso );
		CursosService.parteCompartilhada.emit( curso );
	}

}