import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ICurso } from 'src/app/shared/interface/icurso';
import { CursosService } from '../cursos.service';


@Component( {
	selector: 'app-listagem-cursos',
	templateUrl: './listagem-cursos.component.html',
	styleUrls: [ './listagem-cursos.component.css' ]
} )
export class ListagemCursosComponent implements OnInit
{

	cursos: ICurso[];
	pg: number = 1;
	inscricaoRota: Subscription;

	constructor ( private cursosService: CursosService, private route: ActivatedRoute, private router: Router )
	{
		this.cursos = this.cursosService.getCursos();
		// this.router.navigate( [ '/cursos', this.cursos[0].id ] );
	}

	ngOnInit ()
	{
		// this.router.navigate( [ '/cursos', this.cursos[0].id ] );
		
		// Armazena em um Subscription local para poder destruí-lo no OnDestroy
		this.inscricaoRota = this.route.queryParams.subscribe(
			( queryParams: any ) =>
			{
				if ( typeof ( queryParams[ 'pg' ] ) !== 'undefined' )
				{
					this.pg = queryParams[ 'pg' ];
				}
			}
		);

		// Adicionando informação da instância do serviço que é exclusiva a este componente
		this.cursosService.emitirCursoCriado.subscribe(
			( curso: ICurso ) =>
			{
				console.log( 'EmitirCursoCriado Console.log - CursosComponent: ' + curso.nome + ' - ' + Date.now() )	
			}
		);

		// Adicionando curso compartilhado
		CursosService.parteCompartilhada.subscribe(
			( curso: ICurso ) =>
			{
				console.log( 'CursosService Console.log - CursosComponent: ' + curso.nome + ' - ' + Date.now() );
				this.cursos.push( curso );
			}
		);
		
	}

	proximaPagina ()
	{
		// this.pg++;
		// Interação com QueryParams para Rota imperativa
		this.router.navigate(
			[ '/cursos' ], { queryParams: { 'pg': ++this.pg } }
		);
	}

	ngOnDestroy (): void 
	{
		this.inscricaoRota.unsubscribe();
	}


}
