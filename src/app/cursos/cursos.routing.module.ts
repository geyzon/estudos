import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlunoDetalheComponent } from '../alunos/aluno-detalhe/aluno-detalhe.component';
import { ListagemAlunosComponent } from '../alunos/listagem-alunos/listagem-alunos.component';
import { ReceberCursoCriadoComponent } from '../receber-curso-criado/receber-curso-criado.component';
import { CriarCursoComponent } from './criar-curso/criar-curso.component';
import { CursoDetalheComponent } from './curso-detalhe/curso-detalhe.component';
import { CursoNaoEncontradoComponent } from './curso-nao-encontrado/curso-nao-encontrado.component';
import { ListagemCursosComponent } from './listagem-cursos/listagem-cursos.component';


const routesCursos: Routes = [
	{
		path: 'cursos', component: ListagemCursosComponent, children: [
			{ path: 'recibo', component: ReceberCursoCriadoComponent },
			{ path: 'naoEncontrado', component: CursoNaoEncontradoComponent },
			{ path: ':id', component: CursoDetalheComponent },
			//n{ path: ':pg', component: CursosComponent },
		]
	},
	{
		path: 'alunos', component: ListagemAlunosComponent, children: [
			{ path: 'naoEncontrado', component: CursoNaoEncontradoComponent },
			{ path: ':id', component: AlunoDetalheComponent },
			//n{ path: ':pg', component: CursosComponent },
		]
	},
	{ path: 'criarcurso', component: CriarCursoComponent },
];


@NgModule( {
	imports: [ RouterModule.forChild( routesCursos ) ],
	exports: [ RouterModule ]
} )
export class CursosRoutingModule { }