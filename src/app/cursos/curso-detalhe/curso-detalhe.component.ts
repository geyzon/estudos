import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ICurso } from 'src/app/shared/interface/icurso';
import { CursosService } from '../cursos.service';


@Component( {
	selector: 'app-curso-detalhe',
	templateUrl: './curso-detalhe.component.html',
	styleUrls: [ './curso-detalhe.component.css' ]
} )
export class CursoDetalheComponent implements OnInit
{

	id: number;
	inscricao: Subscription;
	curso: 	ICurso;

	constructor ( private route: ActivatedRoute, private router: Router, private cursosService: CursosService )
	{
		this.buscaCurso();
	}

	ngOnInit ()
	{
		this.buscaCurso();
	}

	private buscaCurso ()
	{
		this.inscricao = this.route.params.subscribe(
			( params: any ) =>
			{
				this.id = params[ 'id' ];
				this.curso = this.cursosService.getCurso( this.id );

				if ( this.curso == null )
				{
					// Redirecionamento
					this.router.navigate( [ '/naoEncontrado' ] );
				}
			}
		);
	}

	ngOnDestroy (): void
	{
		this.inscricao.unsubscribe();
	}

}
