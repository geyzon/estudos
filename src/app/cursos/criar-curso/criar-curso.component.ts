import { Component, OnInit } from '@angular/core';
import { ICurso } from 'src/app/shared/interface/icurso';
import { CursosService } from '../cursos.service';


@Component( {
	selector: 'app-criar-curso',
	templateUrl: './criar-curso.component.html',
	styleUrls: [ './criar-curso.component.css' ],
	// O provimento desse serviço proporciona o compartilhamento do serviço
	providers: [ CursosService ]
} )
export class CriarCursoComponent implements OnInit
{

	cursinhos: ICurso[];

	constructor ( private cursosService: CursosService ) { }

	ngOnInit ()
	{
		this.cursinhos = this.cursosService.getCursos();
	}

	onAddCurso ( curso: string )
	{
		console.log( 'Criando curso pelo CriarCursoComponent' );
		// Essa instância só fica compartilhada por causa do provider
		this.cursosService.addCurso( { id: ( this.cursinhos ).length + 1, nome: curso } );
	}

}
