import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReceberCursoCriadoComponent } from '../receber-curso-criado/receber-curso-criado.component';
import { CriarCursoComponent } from './criar-curso/criar-curso.component';
import { CursoDetalheComponent } from './curso-detalhe/curso-detalhe.component';
import { CursoNaoEncontradoComponent } from './curso-nao-encontrado/curso-nao-encontrado.component';
import { CursosComponent } from './cursos.component';
import { CursosRoutingModule } from './cursos.routing.module';
import { CursosService } from './cursos.service';
import { ListagemCursosComponent } from './listagem-cursos/listagem-cursos.component';


@NgModule( {
	declarations: [
		CursosComponent,
		CursoDetalheComponent,
		CursoNaoEncontradoComponent,
		ListagemCursosComponent,
		CriarCursoComponent,
		ReceberCursoCriadoComponent
	],
	imports: [
		FormsModule,
		CommonModule,
		CursosRoutingModule,
		// RouterModule
	],
	exports: [
		CursosComponent
	],
	providers: [
		CursosService
	],
	bootstrap: [
		CursosComponent
	]
} )
export class CursosModule { }
