import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component( {
	selector: 'app-curso-nao-encontrado',
	templateUrl: './curso-nao-encontrado.component.html',
	styleUrls: [ './curso-nao-encontrado.component.css' ]
} )
export class CursoNaoEncontradoComponent implements OnInit
{

	constructor ( private route: ActivatedRoute ) { }

	ngOnInit ()
	{
	}

}
