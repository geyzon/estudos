import { registerLocaleData } from '@angular/common';
import localeBr from '@angular/common/locales/pt';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AlunosModule } from './alunos/alunos.module';
// import { LogService } from './shared/log.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CicloComponent } from './ciclo/ciclo.component';
import { CursosModule } from './cursos/cursos.module';
import { DatabidingComponent } from './databiding/databiding.component';
import { DiretivasModule } from './diretivas/diretivas.module';
import { FormularioModule } from './formulario/formulario.module';
import { NgcontentComponent } from './ngcontent/ngcontent.component';
import { OperadorElvisComponent } from './operador-elvis/operador-elvis.component';
import { OutputPropertyComponent } from './output-property/output-property.component';
import { PipesModule } from './pipes/pipes.module';
import { BarraNavegacaoComponent } from './shared/componentes/barra-navegacao/barra-navegacao.component';



registerLocaleData( localeBr, 'pt-BR' );

@NgModule( {
	declarations: [
		AppComponent,

		OutputPropertyComponent,
		DatabidingComponent,
		CicloComponent,

		OperadorElvisComponent,
		NgcontentComponent,
		BarraNavegacaoComponent
	],
	imports: [
		AppRoutingModule,
		FormsModule,
		BrowserModule,
		BrowserAnimationsModule,
		CursosModule,
		AlunosModule,
		PipesModule,
		DiretivasModule,
		FormularioModule,
		ReactiveFormsModule
	],
	providers: [
		// LogService
		{
			provide: LOCALE_ID,
			useValue: 'pt-BR'
		}
	],
	bootstrap: [ AppComponent ]
} )
export class AppModule { }
