import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FormDebugComponent } from './../shared/componentes/form-debug/form-debug.component';
import { FormularioComponent } from './formulario.component';
import { PreenchimentoComponent } from './preenchimento/preenchimento.component';



@NgModule( {
	declarations: [
		PreenchimentoComponent,
		FormularioComponent,
		FormDebugComponent
	],
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule,
		HttpClientModule
	],
	exports: [
		FormDebugComponent
	],
	bootstrap: [
		FormularioComponent
	]
} )
export class FormularioModule { }
