import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import * as $ from 'jquery';


@Component( {
	selector: 'app-formulario',
	templateUrl: './formulario.component.html',
	styleUrls: [ './formulario.component.css' ]
} )
export class FormularioComponent implements OnInit
{

	//formulario: FormGroup;
	usuario: any = {
		nome: 'Geyzon Amaral',
		email: 'geyzon@gmail.com'
	}
	
	constructor ( /* private formBuilder: FormBuilder,*/ private httpClient: HttpClient ) { }
	ngOnInit ()
	{
		/*
		this.formulario = this.formBuilder.group( {
			nome: [ null, [Validators.minLength(10)] ],
			email: [ null, [
				Validators.pattern( /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
			] ]
		} );
		*/

	}
	onSubmit (formModel)
	{
		this.httpClient.post( 'https://httpbin.org/post',
			JSON.stringify( formModel.form.value ), { responseType: 'json' } )
			.subscribe(
			dados =>
			{
				console.log( dados );
				this.resetar(formModel);
			}
		);
	}
	resetar (formModel: FormGroup)
	{
		formModel.reset();
	}
	
	salta ( evento, proximo )
	{
		var atributos = ( evento.originalTarget );
		if ( atributos.maxLength === atributos.textLength )
		{
			$( '#' + proximo ).focus();
		}
	} 

}
 