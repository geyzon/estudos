import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreenchimentoComponent } from './preenchimento.component';

describe('PreenchimentoComponent', () => {
  let component: PreenchimentoComponent;
  let fixture: ComponentFixture<PreenchimentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreenchimentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreenchimentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
