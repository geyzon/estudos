import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-component-databiding',
	templateUrl: './databiding.component.html',
	styleUrls: ['./databiding.component.css'],
})
export class DatabidingComponent implements OnInit {

	public nomeDoCurso = 'Angular';

	// @Input() public valorInicial: number; ou
	public valorInicial = 15;

	constructor() { }

	public onMudouValor(evento) {
		console.log(evento.novoValor);
	}

	public ngOnInit() {
	}

}
