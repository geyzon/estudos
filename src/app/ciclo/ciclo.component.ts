import {
	AfterContentChecked,
	AfterContentInit,
	AfterViewChecked,
	AfterViewInit,
	Component,
	DoCheck,
	Input,
	OnChanges,
	OnInit,
} from '@angular/core';

@Component({
	selector: 'app-component-ciclo',
	templateUrl: './ciclo.component.html',
	styleUrls: ['./ciclo.component.css'],
})
export class CicloComponent implements OnInit /*, OnChanges, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked*/ {

// @Input() public valorInicial: number; ou
	@Input() public valorInicial = 10;

	constructor() {
		this.log('constructor');
	}

	public ngOnInit() {
		this.log('onInit');
	}

	/*
	public ngOnChanges() {
		this.log('onChanges');
	}

	public ngDoCheck() {
		this.log('doCheck');
	}

	public ngAfterContentInit() {
		this.log('afterContentInit');
	}

	public ngAfterContentChecked() {
		this.log('afterContentChecked');
	}

	public ngAfterViewInit() {
		this.log('afterViewInit');
	}

	public ngAfterViewChecked() {
		this.log('afterViewChecked');
	}

	public ngDestroy() {
		this.log('onDestroy');
	}
	*/
	private log(hook: string) {
		console.log(hook);
	}
}
