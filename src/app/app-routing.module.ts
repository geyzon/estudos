import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { DatabidingComponent } from './databiding/databiding.component';
import { FormularioComponent } from './formulario/formulario.component';
import { PreenchimentoComponent } from './formulario/preenchimento/preenchimento.component';


const routes: Routes = [
	{ path: 'formulario', component: FormularioComponent },
	{ path: 'formulario/preenchimento', component: PreenchimentoComponent },
	{ path: 'dados', component: DatabidingComponent },
	{ path: '', component: AppComponent },
];

@NgModule( {
	imports: [ RouterModule.forRoot( routes ) ],
	exports: [ RouterModule ]
} )
export class AppRoutingModule { }