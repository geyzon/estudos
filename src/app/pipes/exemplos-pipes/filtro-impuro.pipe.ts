import { Pipe } from '@angular/core';
import { FiltroArrayPipe } from './filtro-array.pipe';

@Pipe({
	name: 'filtroImpuro'
})
export class FiltroImpuroPipe extends FiltroArrayPipe {

	transform(value: any, ...args: any[]): any {
		return null;
	}

}