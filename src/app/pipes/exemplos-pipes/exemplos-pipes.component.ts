import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-exemplos-pipes',
	templateUrl: './exemplos-pipes.component.html',
	styleUrls: ['./exemplos-pipes.component.css']
})
export class ExemplosPipesComponent implements OnInit {

	filtro = '';

	livro: any = {
		titulo: 'LEARNING JAVAScript in 24 HOURS',
		rating: 4.5432,
		numeroPaginas: 314,
		preco: 44.99,
		dataLancamento: new Date(2016, 5, 23),
		url: 'http://a.co/glqjpRP'
	};

	livros = ['Java 2', 'JavaScript Básico', 'Angular 3 - Completo'];

	constructor() { }

	ngOnInit() {
	}

}
