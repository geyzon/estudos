import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CamelCasePipe } from './exemplos-pipes/camel-case.pipe';
import { ExemplosPipesComponent } from './exemplos-pipes/exemplos-pipes.component';
import { FiltroArrayPipe } from './exemplos-pipes/filtro-array.pipe';
import { FiltroImpuroPipe } from './exemplos-pipes/filtro-impuro.pipe';


@NgModule({
	declarations: [
		ExemplosPipesComponent,
		
		CamelCasePipe,
		FiltroArrayPipe,
		FiltroImpuroPipe
	],
	imports: [
		CommonModule,
		FormsModule
	]
})
export class PipesModule { }
