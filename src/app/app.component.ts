import { Component } from '@angular/core';

@Component( {
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: [ './app.component.css' ],
	preserveWhitespaces: true
} )
export class AppComponent
{
	public title = 'Estudos';

	// public valor: number; ou
	public valor = 15;

	// public deletarCiclo: boolean; ou
	public deletarCiclo = false;

	public crescerValor ()
	{
		this.valor++;
	}

	public destruirCiclo ()
	{
		this.deletarCiclo = true;
	}
}
