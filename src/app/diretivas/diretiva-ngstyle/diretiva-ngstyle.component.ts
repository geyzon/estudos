import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-diretiva-ngstyle',
  templateUrl: './diretiva-ngstyle.component.html',
  styleUrls: ['./diretiva-ngstyle.component.sass']
})
export class DiretivaNgstyleComponent implements OnInit {

  ativo = false;

  tamanhoFonte = 14;

  constructor() { }

  ngOnInit() {
  }

  mudaEstilo() {
		this.ativo = !this.ativo;
	}

}
