import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-diretiva-ngclass',
	templateUrl: './diretiva-ngclass.component.html',
	styleUrls: ['./diretiva-ngclass.component.sass']
})
export class DiretivaNgclassComponent implements OnInit {

	transporte = false;

	constructor() { }

	ngOnInit() {
	}

	veiculo() {
		this.transporte = !this.transporte;
		console.log(this.transporte);
	}

}
