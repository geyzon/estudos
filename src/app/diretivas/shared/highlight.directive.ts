import { Directive, HostListener, HostBinding, Input } from '@angular/core';

@Directive({
	selector: '[dtHighlight]'
})
export class HighlightDirective {

	@Input() defaultColor: string = 'white';
	@Input('highlight') highlightColor: string = 'yellow';

	// Binding entre a propriedade 'style.backgroundColor' e a variável backgroundColor
	@HostBinding('style.backgroundColor')
	backgroundColor: string;

	@HostListener('mouseover')
	onMouseOver()
	{
		this.backgroundColor = this.defaultColor;
	}

	@HostListener('mouseleave')
	onMouseLeave()
	{
		this.backgroundColor = this.highlightColor;
	}

	constructor() { }

	ngOnInit() {
		// Ao iniciar a aplicação, eu seto o background com a nova cor default atribuída na carga
		this.backgroundColor = this.defaultColor;
	}
}
