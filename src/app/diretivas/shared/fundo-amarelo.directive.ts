import { Directive, ElementRef, Renderer2 } from '@angular/core';

@Directive({
	selector: '[dtFundoAmarelo]'
})
export class FundoAmareloDirective {

	constructor(private _elementRef: ElementRef, private _renderer: Renderer2) {

		const elemento = this._elementRef.nativeElement; // Formato que acarreta em funerabilidades - Captura todos os elementos com tal diretiva
		const objeto = this._renderer;
		elemento.style.color = 'black';
		elemento.style.backgroundColor = 'yellow'; // this._elementRef.nativeElement.style = "background-color: yellow"

		// Maneira segura de alterar propriedades dos elementos
		this._renderer.setStyle(elemento, 'background-color', 'cyan');
	}

}
