import { Directive, ElementRef, HostBinding, HostListener, Renderer2 } from '@angular/core';
// import { ProceduralRenderer3, RendererStyleFlags3 } from '@angular/core/src/render3/interfaces/renderer';

@Directive({
	selector: '[dtHighlightMouse]'
})
export class HighlightMouseDirective
{
	// Binding entre a propriedade 'style.backgroundColor' e a variável backgroundColor
	@HostBinding('style.backgroundColor')
	backgroundColor: string;

	// Poderia também ter o seguinte
	/*
	private backgroundColor: string;
	@HostBinding('style.backgroundColor') get setColor() {
		// Qualquer código para manipular a variável antes de setá-la para o binding
		return this.backgroundColor;
	}
	*/

	@HostListener('mouseover')
	onMouseOver()
	{
		/*
		this._renderer.setStyle(
			this._elementRef.nativeElement,
			'background-color',
			'yellow'
		)
		*/
		this.backgroundColor = 'yellow';
	}

	@HostListener('mouseleave')
	onMouseLeave()
	{
		/*
		this._renderer.setStyle(
			this._elementRef.nativeElement,
			'background-color',
			'white'
		);
		*/
		this.backgroundColor = 'white';
	}

	constructor(private _elementRef: ElementRef, private _renderer: Renderer2) { }

}
