import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
	selector: '[dtNgElse]'
})
export class NgElseDirective {

	private _hasView = false;

	@Input() set dtNgElse(condition: boolean)
	{
		if (!condition && this._hasView) {
			// Para renderizar um templete numa diretiva estrutural
			this._viewContainer.createEmbeddedView(this._template);
		} else if (condition && !this._hasView) {
			this._hasView = false;
			// Elimina o elemento se a condição não for aceita
			this._viewContainer.clear();
		}
	}

	constructor(private _template: TemplateRef<any>, private _viewContainer: ViewContainerRef) { }

}
