import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DiretivaNgclassComponent } from './diretiva-ngclass/diretiva-ngclass.component';
import { DiretivaNgforComponent } from './diretiva-ngfor/diretiva-ngfor.component';
import { DiretivaNgifComponent } from './diretiva-ngif/diretiva-ngif.component';
import { DiretivaNgstyleComponent } from './diretiva-ngstyle/diretiva-ngstyle.component';
import { DiretivaNgswitchComponent } from './diretiva-ngswitch/diretiva-ngswitch.component';
import { DiretivasCustomizadasComponent } from './diretivas-customizadas/diretivas-customizadas.component';

import { FundoAmareloDirective } from './shared/fundo-amarelo.directive';
import { HighlightMouseDirective } from './shared/highlight-mouse.directive';
import { HighlightDirective } from './shared/highlight.directive';
import { NgElseDirective } from './shared/ng-else.directive';


@NgModule({
	declarations: [
		DiretivaNgifComponent,
		DiretivaNgswitchComponent,
		DiretivaNgforComponent,
		DiretivaNgclassComponent,
		DiretivaNgstyleComponent,
		DiretivasCustomizadasComponent,
		
		FundoAmareloDirective,
		HighlightMouseDirective,
		HighlightDirective,
		NgElseDirective
  ],
  imports: [
    CommonModule
  ]
})
export class DiretivasModule { }
