import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

@Component({
	selector: 'app-component-contador',
	templateUrl: './output-property.component.html',
	styleUrls: ['./output-property.component.css'],
})
export class OutputPropertyComponent implements OnInit {

	// @Input() public valor: number; ou
	@Input() public valor = 0;

	@Output() public mudouValor = new EventEmitter();

	@ViewChild('campoInput', { read: ElementRef, static: false }) public campoValorInput: ElementRef;

	constructor() { }

	public decrementa() {
		this.campoValorInput.nativeElement.value--;
		this.mudouValor.emit({ novoValor: this.campoValorInput.nativeElement.value });
	}

	public incrementa() {
		this.campoValorInput.nativeElement.value++;
		this.mudouValor.emit({ novoValor: this.campoValorInput.nativeElement.value });
	}

	public ngOnInit() {
	}

}
