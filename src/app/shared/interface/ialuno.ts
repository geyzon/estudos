export interface IAluno {
	id: number;
	nome: string;
	idade: number;
}
