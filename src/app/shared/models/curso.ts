export class Curso
{
	private _id: number;
	private _nome: string;

	constructor ( id: number, nome: string )
	{
		this.id = id;
		this.nome = nome;
	}

	public get id ()
	{
		return this._id;
	}
	public set id ( id: number )
	{
		this._id = id;
	}

	public get nome ()
	{
		return this._nome;
	}
	public set nome ( nome: string )
	{
		this._nome = nome;
	}
}
