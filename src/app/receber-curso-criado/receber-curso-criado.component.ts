import { Component, OnInit } from '@angular/core';
import { CursosService } from '../cursos/cursos.service';
import { Curso } from '../shared/models/curso';


@Component({
	selector: 'app-receber-curso-criado',
	templateUrl: './receber-curso-criado.component.html',
	styleUrls: ['./receber-curso-criado.component.css']
})
export class ReceberCursoCriadoComponent implements OnInit {

	curso: Curso;

	constructor(private cursosService: CursosService) { }

	ngOnInit() {
		this.cursosService.emitirCursoCriado.subscribe(
			(curso: Curso) => this.curso = curso
		);
		// O uso da serviço estático evita necessidade de provimento pelo provider
		CursosService.parteCompartilhada.subscribe(
			(cursoCriado: Curso) => console.log(cursoCriado.nome + ' - ' + Date.now())
		);
	}

}
