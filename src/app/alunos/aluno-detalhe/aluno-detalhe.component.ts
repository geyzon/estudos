import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { IAluno } from 'src/app/shared/interface/ialuno';
import { AlunosService } from '../alunos.service';

@Component({
  selector: 'app-aluno-detalhe',
  templateUrl: './aluno-detalhe.component.html',
  styleUrls: ['./aluno-detalhe.component.css']
})
export class AlunoDetalheComponent implements OnInit {

	id: number;
	inscricao: Subscription;
	aluno: IAluno;

	constructor ( private route: ActivatedRoute, private router: Router, private alunosService: AlunosService )
	{
		this.buscaAlunos();
	}

	ngOnInit ()
	{
		this.buscaAlunos();
	}

	private buscaAlunos ()
	{
		this.inscricao = this.route.params.subscribe(
			( params: any ) =>
			{
				this.id = params[ 'id' ];
				this.aluno = this.alunosService.getAluno( this.id );

				if ( this.aluno == null )
				{
					// Redirecionamento
					this.router.navigate( [ '/naoEncontrado' ] );
				}
			}
		);
	}

	ngOnDestroy (): void
	{
		this.inscricao.unsubscribe();
	}

}
