import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AlunoDetalheComponent } from './aluno-detalhe/aluno-detalhe.component';
import { AlunosComponent } from './alunos.component';
import { AlunosRoutingModule } from './alunos.routing.module';
import { AlunosService } from './alunos.service';
import { ListagemAlunosComponent } from './listagem-alunos/listagem-alunos.component';


@NgModule( {
	declarations: [
		AlunosComponent,
		AlunoDetalheComponent,
		ListagemAlunosComponent
	],
	imports: [
		CommonModule,
		AlunosRoutingModule
	],
	exports: [],
	providers: [ AlunosService ]
} )
export class AlunosModule { }
