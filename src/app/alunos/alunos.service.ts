import { EventEmitter, Injectable } from '@angular/core';
import { IAluno } from '../shared/interface/ialuno';
import { LogService } from '../shared/services/log.service';


@Injectable()
export class AlunosService
{

	static parteCompartilhada = new EventEmitter<IAluno>();
	emitirAlunoCriado = new EventEmitter<IAluno>();

	// Inicializar a variável é muito importante
	private alunos: IAluno[] = [
		{ id: 1, nome: 'Paulo Silveira', idade: 25 },
		{ id: 2, nome: 'Lúcia Camargo', idade: 19 },
		{ id: 3, nome: 'Valdemar Figueiredo', idade: 33 }
	];


	constructor ( protected logService: LogService )
	{
		console.log( 'Recebendo chamado...' );
		this.logService.consoleLog( 'Log personalizado monitorando...' );
	}

	getAlunos (): IAluno[]
	{
		this.logService.consoleLog( 'Alunos sendo requisitados!' );
		return this.alunos;
	}

	getAluno ( id: number ): IAluno
	{
		let aluno: IAluno;
		for ( let i = 0; i < this.alunos.length; i++ )
		{
			aluno = this.alunos[ i ];
			if ( aluno.id == id )
			{
				return aluno;
			}
		}
	}

	addAluno ( aluno: IAluno )
	{
		this.logService.consoleLog( 'this.logService - AlunosService: ' + aluno.nome );
		console.log( 'Console.log - AlunosService: ' + aluno.nome );
		this.alunos.push( aluno );
		this.emitirAlunoCriado.emit( aluno );
		AlunosService.parteCompartilhada.emit( aluno );
	}

}