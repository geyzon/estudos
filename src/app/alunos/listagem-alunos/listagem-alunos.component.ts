import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AlunosService } from 'src/app/alunos/alunos.service';
import { IAluno } from 'src/app/shared/interface/ialuno';



@Component({
  selector: 'app-listagem-alunos',
  templateUrl: './listagem-alunos.component.html',
  styleUrls: ['./listagem-alunos.component.css']
} )
	
export class ListagemAlunosComponent implements OnInit {

	alunos: IAluno[];
	pg: number = 1;
	inscricaoRota: Subscription;

	constructor ( private alunosService: AlunosService, private route: ActivatedRoute, private router: Router )
	{
		this.alunos = this.alunosService.getAlunos();
	}

	ngOnInit ()
	{

		// Armazena em um Subscription local para poder destruí-lo no OnDestroy
		this.inscricaoRota = this.route.queryParams.subscribe(
			( queryParams: any ) =>
			{
				if ( typeof ( queryParams[ 'pg' ] ) !== 'undefined' )
				{
					this.pg = queryParams[ 'pg' ];
				}
			}
		);

		// Adicionando informação da instância do serviço que é exclusiva a este componente
		this.alunosService.emitirAlunoCriado.subscribe(
			( aluno: IAluno ) => console.log( 'EmitirAlunoCriado Console.log - AlunosComponent: ' + aluno.nome + ' - ' + Date.now() )
		);

		// Adicionando curso compartilhado
		AlunosService.parteCompartilhada.subscribe(
			( aluno: IAluno ) =>
			{
				console.log( 'AlunosService Console.log - AlunosComponent: ' + aluno.nome + ' - ' + Date.now() );
				this.alunos.push( aluno );
			}
		);
	}

	proximaPagina ()
	{
		// this.pg++;
		// Interação com QueryParams para Rota imperativa
		this.router.navigate(
			[ '/alunos' ], { queryParams: { 'pg': ++this.pg } }
		);
	}

	ngOnDestroy (): void 
	{
		this.inscricaoRota.unsubscribe();
	}


}
