import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlunoDetalheComponent } from './aluno-detalhe/aluno-detalhe.component';
import { AlunosComponent } from './alunos.component';


const routesAlunos: Routes = [ 
	{ 
		path: 'alunos', component: AlunosComponent, children: [
			{ path: ':id', component: AlunoDetalheComponent },
		]
	}
];


@NgModule( {
	imports: [RouterModule.forRoot ( routesAlunos ) ],
	exports: [ RouterModule ]
})
export class AlunosRoutingModule { }